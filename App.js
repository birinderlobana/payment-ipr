import React from 'react';

import Routes from './Routes';
import SplashView from './components/SplashView';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { login: false };
    this.onLogin = this.onLogin.bind(this);
  }
  onLogin() {
    this.setState({
      login: true,
      haha: false,
    });
  }
  render() {
    if (!this.state.login) {
      return <SplashView handleLogin={this.onLogin} />;
    }
    return <Routes />;
  }
}
