import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { Tile, List, ListItem } from 'react-native-elements';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
  Animated,
} from 'react-native';
import Stripe from 'react-native-stripe-api';


class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: "",
      expmonth: "",
      expyear: "",
      cvc: ""
    };
  }

  handlePayment(){
    const apiKey = 'sk_test_6VKAjOAWOX7MIgdMV3jBqcxl';
    const client = new Stripe(apiKey);
    const token = client.createToken({
       number: '4242424242424242' ,
       exp_month: '09',
       exp_year: '18',
       cvc: '111',
       address_zip: '12345'
    });
  }

  render() {
    const picture = { large: '' };
    const name = { first: 'first', last: 'last' };
    const email = 'test@test.test';
    const phone = '000-000-0000';
    const login = { username: 'username' };
    const dob = '2020/1/1';
    const location = { city: 'Toronto' };

    return (
      <ScrollView>
        <Tile
          imageSrc={{ uri: picture.large }}
          featured
          title={`${name.first.toUpperCase()} ${name.last.toUpperCase()}`}
          caption={email}
        />
        <List>
          <ListItem title="Email" rightTitle={email} hideChevron />
          <ListItem title="Phone" rightTitle={phone} hideChevron />
        </List>
        <List>
          <ListItem title="Username" rightTitle={login.username} hideChevron />
        </List>

        <List>
          <ListItem title="Birthday" rightTitle={dob} hideChevron />
          <ListItem title="City" rightTitle={location.city} hideChevron />
        </List>
        <TextInput
          onChangeText={(number) => this.setState({number})}
          placeholder="Credit Card Number"
          placeholderTextColor="rgba(128,128,128,0.5)"
          returnKeyType="next"
          style={styles.input}
          keyboardType="Number"
          autoCapitalize="none"
          autoCorrect={false}
        />
        <TextInput
          onChangeText={(expmonth) => this.setState({expmonth})}
          placeholder="Month of Expiry"
          placeholderTextColor="rgba(128,128,128,0.5)"
          returnKeyType="next"
          style={styles.input}
          keyboardType="Number"
          autoCapitalize="none"
          autoCorrect={false}
        />
        <TextInput
          onChangeText={(expyear) => this.setState({expyear})}
          placeholder="Year of Expiry"
          placeholderTextColor="rgba(128,128,128,0.5)"
          returnKeyType="next"
          style={styles.input}
          keyboardType="Number"
          autoCapitalize="none"
          autoCorrect={false}
        />
        <TextInput
          onChangeText={(cvc) => this.setState({cvc})}
          placeholder="CVC"
          placeholderTextColor="rgba(128,128,128,0.5)"
          returnKeyType="next"
          style={styles.input}
          keyboardType="Number"
          autoCapitalize="none"
          autoCorrect={false}
        />
        <TouchableOpacity style={styles.button} onPress={this.handlePayment.bind(this)}>
          <Text style={styles.buttonContent}>Submit</Text>
        </TouchableOpacity>
      </ScrollView>



    );
  }
}
const styles = StyleSheet.create({
  /*container: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    fontWeight: 'bold',
    paddingVertical: 30,
  },*/
  input: {
    height: 50,
    backgroundColor: 'rgba(0,0,0,0)',
    marginBottom: 10,
    color: 'black',
    paddingHorizontal: 10,
  },

  button: {
    backgroundColor: '#8BC34A',
    paddingVertical: 12,
  },
  buttonContent: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
});


export default Profile;
