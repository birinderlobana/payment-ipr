import React from 'react';
import MapView from 'react-native-maps';
import { StyleSheet, Text, View, TextInput, Image, Button } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Icon } from 'react-native-elements';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.handleMapSearch = this.handleMapSearch.bind(this);
    this.state = {
      text: '',
      markers: marker,
    };
  }
  handleMapSearch() {
    console.log('searching hahah', this.state.text);
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.searchBox}
          placeholder="search"
          placeholderTextColor="rgba(0,0,0,0.4)"
          autoCorrect={false}
          onChangeText={text => this.setState({ text })}
          value={this.state.text}
          onSubmitEditing={() => this.handleMapSearch}
        />
        {/* <GooglePlacesInput style={styles.searchBox} /> */}
        <View style={styles.menuButton}>
          <Icon raised onPress={() => navigate('DrawerOpen')} name="more-horiz" />
        </View>
        <MapView
          provider="google"
          showsUserLocation
          showsMyLocationButton
          style={styles.map}
          initialRegion={{
            latitude: 43.66259,
            longitude: -79.393886,
            latitudeDelta: 0.0622,
            longitudeDelta: 0.0281,
          }}
        >
          {this.state.markers.map(marker => (
            <MapView.Marker
              coordinate={marker.latlng}
              title={marker.title}
              description={marker.description}
            >
              <MarkerIcon tag={marker.tag} />
            </MapView.Marker>
          ))}
        </MapView>
      </View>
    );
  }
}
const marker = [
  {
    latlng: {
      latitude: 43.66259,
      longitude: -79.393886,
    },
    title: 'test',
    description: '$1/sec',
    tag: 5,
  },
  {
    latlng: {
      latitude: 43.66259,
      longitude: -79.399886,
    },
    title: 'cheap',
    description: '$2/year',
    tag: 1,
  },
  {
    latlng: {
      latitude: 43.66559,
      longitude: -79.396886,
    },
    title: 'abc',
    description: '$200/month',
    tag: 9,
  },
];
const MarkerIcon = ({ tag }) => (
  <View style={{ textAlign: 'center', width: 33, height: 43 }}>
    <Image
      style={{ flex: 1, resizeMode: 'center', width: 33, height: 43 }}
      source={require('../assets/marker.png')}
    >
      <Text style={{ fontWeight: '600', paddingLeft: 12, paddingTop: 7, color: '#696969' }}>{ tag }</Text>
    </Image>
  </View>
);
const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    zIndex: -1,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  menuButton: {
    position: 'absolute',
    bottom: 80,
    right: 5,
  },
  searchBox: {
    alignSelf: 'stretch',
    zIndex: 0,
    height: 40,
    marginVertical: 40,
    marginHorizontal: 20,
    paddingHorizontal: 20,
    backgroundColor: 'white',
  },
});

const homePlace = {
  description: 'Home',
  geometry: { location: { lat: 48.8152937, lng: 2.4597668 } },
};
const workPlace = {
  description: 'Work',
  geometry: { location: { lat: 48.8496818, lng: 2.2940881 } },
};

const GooglePlacesInput = () => (
  <GooglePlacesAutocomplete
    placeholder="Search"
    minLength={2} // minimum length of text to search
    autoFocus={false}
    returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
    listViewDisplayed="auto" // true/false/undefined
    fetchDetails
    renderDescription={row => row.description} // custom description render
    onPress={(data, details = null) => {
      // 'details' is provided when fetchDetails = true
      console.log(data);
      console.log(details);
    }}
    getDefaultValue={() => '' // text input default value
    }
    query={{
      // available options: https://developers.google.com/places/web-service/autocomplete
      key: 'YOUR API KEY',
      language: 'en', // language of the results
      types: '(cities)', // default: 'geocode'
    }}
    styles={{
      description: {
        fontWeight: 'bold',
      },
      predefinedPlacesDescription: {
        color: '#1faadb',
      },
    }}
    currentLocation // Will add a 'Current location' button at the top of the predefined places list
    currentLocationLabel="Current location"
    nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
    GoogleReverseGeocodingQuery={{
      // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
    }}
    GooglePlacesSearchQuery={{
      // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
      rankby: 'distance',
      types: 'food',
    }}
    filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
    predefinedPlaces={[homePlace, workPlace]}
    debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
    renderLeftButton={() => <Text>LeftIcon</Text>}
    renderRightButton={() => <Text>Custom text after the inputg</Text>}
  />
);
