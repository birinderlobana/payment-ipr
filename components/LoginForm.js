import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
  Animated,
} from 'react-native';

export default class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.state = {
      fadeAnim: new Animated.Value(0),
    };
  }

  componentDidMount() {
    Animated.timing(
      // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 1, // Animate to opacity: 1 (opaque)
        duration: 800, // Make it take a while
      },
    ).start();
  }
  handleLogin() {
    Animated.timing(
      // Animate over time
      this.state.fadeAnim, // The animated value to drive
      {
        toValue: 0, // Animate to opacity: 1 (opaque)
        duration: 500, // Make it take a while
      },
    ).start(()=>this.props.handleLogin());
  }
  render() {
    const { fadeAnim } = this.state;
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <Animated.View style={{ opacity: fadeAnim }}>
          <StatusBar barStyle="light-content" />
          <View style={styles.titleWrapper}>
            <Text style={styles.title}>GRID Parking</Text>
          </View>
          <TextInput
            placeholder="username"
            placeholderTextColor="rgba(255,255,255,0.8)"
            returnKeyType="next"
            style={styles.input}
            keyboardType="email-address"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => this.passwordInput.focus()}
          />
          <TextInput
            placeholder="password"
            placeholderTextColor="rgba(255,255,255,0.8)"
            returnKeyType="go"
            secureTextEntry
            style={styles.input}
            ref={el => (this.passwordInput = el)}
          />
          <TouchableOpacity style={styles.button} onPress={this.handleLogin}>
            <Text style={styles.buttonContent}>LOGIN</Text>
          </TouchableOpacity>
        </Animated.View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    color: 'white',
    fontWeight: 'bold',
    paddingVertical: 30,
  },
  input: {
    height: 50,
    backgroundColor: 'rgba(255,255,255,0.3)',
    marginBottom: 10,
    color: 'white',
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: '#8BC34A',
    paddingVertical: 12,
  },
  buttonContent: {
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
});
