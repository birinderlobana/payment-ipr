import React, { Component } from 'react';
import { StyleSheet, Text, View, KeyboardAvoidingView } from 'react-native';
import LoginForm from './LoginForm';

export default class Splash extends Component {
  render() {
    return (
      <View style={styles.container}>
        <LoginForm handleLogin={this.props.handleLogin} />
        <Text style={styles.subtitle}>Prototype Powered By React Native</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4CAF50',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 30,
    color: 'white',
    fontWeight: 'bold',
  },
  subtitle: {
    color: 'white',
    fontSize: 10,
    padding: 20,
  },
  titleWrapper: {
    flex: 1,
    justifyContent: 'center',
  },
});
